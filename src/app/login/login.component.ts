import { Component, OnInit } from '@angular/core';
import Student from '../entity/student';
import Teacher from '../entity/teacher';
import { TeacherService } from '../service/teacher-service';
import { StudentService } from '../service/student-service';
import { ActivatedRoute, Router } from '@angular/router';
import { NavService } from '../service/nav-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginStudentCheck: boolean;
loginTeacherCheck: boolean;
model: Student = new Student();
ngOnInit() {
}

constructor(private route: ActivatedRoute, private router: Router, private studentService: StudentService, private nav: NavService) {
  this.loginStudentCheck = false;
  this.loginTeacherCheck = false;
}

// onSubmitStudent() {
//   this.studentService.getStudentEmailAndPassword(this.model.username, this.model.password)
//   .subscribe((student) => {
//     if (student.id != null) {
//       this.nav.showStudent();
//       this.nav.Login();
//       this.nav.setStudent(student.id, student.studentId, student.name, student.surname, student.birthday, student.image);
//       this.router.navigate(['']);
//     } else {
//       alert('Can not find your account');
//     }
//   }, (error) => {
//     alert('Could not save value');
//   });
// }
// loginStudent() {
//   this.loginStudentCheck = true;
//   this.loginTeacherCheck = false;
// }
// loginTeacher() {
//   this.loginStudentCheck = false;
//   this.loginTeacherCheck = true;
// }
}
