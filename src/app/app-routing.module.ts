import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { TeachersComponent } from './teachers/teachers.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  //  { path: '**', component: FileNotFoundComponent }
  // { path: 'teacher', component: TeachersComponent} ใส่path ว่าจะให้โชว์หน้าไหน

//  { path: 'teachers', component: TeachersComponent }
];

@NgModule ({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
  RouterModule
  ]
})
export class AppRoutingModule {

}
