export default class Teacher {
  id: number;
  name: string;
  surname: string;
  image: string;
  username: string;
  password: string;
}
