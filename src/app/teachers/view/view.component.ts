import { Component, OnInit } from '@angular/core';
import { Teacher } from 'src/app/entity/teacher';
import { ActivatedRoute, Params } from '@angular/router';
import { TeacherService } from 'src/app/service/teacher-service';
import { TeacherDataImplService } from 'src/app/service/teacher-data-impl.service';

@Component ({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
    teachers: Teacher[];

  constructor(private route: ActivatedRoute, private teacherService: TeacherDataImplService) { }
  ngOnInit() {
  // this.teacherService.getTeachers()
  //  .subscribe(teachers => this.teachers = teachers);

    this.teachers = {
      'id': 11,
      'teacherId': '',
      'name': '',
      'surname': '',
      'subject': '',
      'image': ''
   };

    this.route.params
    .subscribe((params: Params) => {
      this.teacherService.getTeacher(params['id'])
      .subscribe((inputTeacher: Teacher) => this.teacher = inputTeacher);
    });
}

}
