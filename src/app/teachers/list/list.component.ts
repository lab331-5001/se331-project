import { Component, OnInit } from '@angular/core';
import { Teacher } from 'src/app/entity/teacher';
import { TeacherService } from 'src/app/service/teacher-service';
import { ActivatedRoute } from '@angular/router';
import { TeacherDataImplService } from 'src/app/service/teacher-data-impl.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  teachers: Teacher[];

  constructor(private teacherService: TeacherDataImplService) { }

  ngOnInit() {
    this.teacherService.getTeachers()
    .subscribe(teachers => this.teachers = teachers);
  }

  // constructor() { }

  // ngOnInit() {
  // }

}
