import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityComponent } from '../teachers/activity/activity.component';
import { ListComponent } from '../teachers/list/list.component';
import { ViewComponent } from './view/view.component';
import { TeacherTableComponent } from './teacher-table/teacher-table.component';
// import { TeachersComponent } from './teachers.component';

const TeacherRoutes: Routes = [
  { path: 'activity', component: ActivityComponent },
  { path: 'list', component: TeacherTableComponent },
  { path: 'detail/:id', component: ViewComponent },
  { path: 'table', component: TeacherTableComponent },
  // { path: 'activitylist', component: AclistTableComponent },
];
@NgModule ({
  imports: [
    RouterModule.forRoot(TeacherRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TeacherRoutingModule {
}

