// import { Component, OnInit, ViewChild } from '@angular/core';
// import { MatPaginator, MatSort } from '@angular/material';
// // import { Activity } from 'src/app/entity/activity';
// import { BehaviorSubject } from 'rxjs';


// @Component({
//   selector: 'app-aclist-table',
//   templateUrl: './aclist-table.component.html',
//   styleUrls: ['./aclist-table.component.css']
// })
// export class AclistTableComponent implements OnInit {
//   @ViewChild(MatPaginator) paginator: MatPaginator;
//   @ViewChild(MatSort) sort: MatSort;
//   dataSource: AclistTableDataSource;
//   /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
//   displayedColumns = ['id', 'activityId', 'name', 'location', 'description', 'period-reg',
//    'date', 'time', 'time-start', 'time-end', 'teacher'];
//   activitys: Activity[];
//   filter: string;
//   filter$: BehaviorSubject<string>;
//   constructor(private activityService: ActivityService) { }
//   ngOnInit() {
//     this.activityService.getActivitys()
//       .subscribe(activitys => {
//         this.dataSource = new AclistTableDataSource(this.paginator, this.sort);
//         this.dataSource.data = activitys;
//         this.activitys = activitys;
//         this.filter$ = new BehaviorSubject<string>('');
//         this.dataSource.filter$ = this.filter$;
//       }
//       );
//   }
//   applyFilter(filterValue: string) {
//     this.filter$.next(filterValue.trim().toLowerCase());
//   }


// }
