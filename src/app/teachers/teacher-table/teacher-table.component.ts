import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { Teacher } from 'src/app/entity/teacher';
import { BehaviorSubject } from 'rxjs';
import { TeacherService } from 'src/app/service/teacher-service';
import { TeacherTableDataSource } from './teacher-table-datasource';

@Component({
  selector: 'app-teacher-table',
  templateUrl: './teacher-table.component.html',
  styleUrls: ['./teacher-table.component.css']
})
export class TeacherTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TeacherTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'teacherId' , 'name', 'surname', 'subject', 'image'];
  teachers: Teacher[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private teacherService: TeacherService) { }
  ngOnInit() {
    this.teacherService.getTeachers()
      .subscribe(teachers => {
        this.dataSource = new TeacherTableDataSource(this.paginator, this.sort);
        this.dataSource.data = teachers;
        this.teachers = teachers;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }


}
