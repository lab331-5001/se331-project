import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminRegisterActivityComponent } from './registerActivity/regisActivity.component';
import { ActivityTableComponent } from './activity-table/activity-table.component';

const ActivityRoutes: Routes = [
    { path: 'listActivity', component: ActivityTableComponent },
    { path: 'registerActivity', component: AdminRegisterActivityComponent },
    { path: 'detail/:id', component: ActivityTableComponent }
];
@NgModule({
    imports: [
        RouterModule.forRoot(ActivityRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ActivityRoutingModule {

}
