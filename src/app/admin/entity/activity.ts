export default class Activity {
    id:number;
    activityId:string;
    name:string;
    location:string;
    description:string;
    period_reg:string;
    date:string;
    time:number;
    teacher:string;
  
  }