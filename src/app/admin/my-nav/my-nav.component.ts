import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import Activity from '../entity/activity';
import { ActivityService } from '../service/activity-service';


@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {

  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
    
    
  constructor(private breakpointObserver: BreakpointObserver, private activityService: ActivityService) { }
   activity$: Observable<Activity[]> = this.activityService.getActivitys();

  }