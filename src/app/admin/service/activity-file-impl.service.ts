import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivityService } from './activity-service';
import { map } from 'rxjs/internal/operators/map';
import Activity from '../entity/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityFileImplService extends ActivityService{
  saveActivity(activity: Activity): Observable<Activity> {
    this.activitys.push(activity);
    console.log(this.activitys);
    return of(activity);
  }
  constructor(private http: HttpClient) { 
    super();
  }
  getActivitys(): Observable<Activity[]> {
    return of(this.activitys);
  };
  getActivity(id: number): Observable<Activity> {
    return of(this.activitys)
      .pipe(map(activitys =>{
        const output: Activity = (activitys as Activity[]).find(activity => activity.id === +id);
        return output;
      }));
  }
  activitys: Activity[] =
  [
    {
          "id": 1,
          "activityId": "AC-001",
          "name": "Leaning Activity 1",
          "location": "Camt_113",
          "date": "Wenesday",
          "time": 13.00,
          "teacher": "TK",
          "period_reg": "20 oct 2018",
          "description": "Lean every Wenesday"
        },
         {
            "id": 2,
          "activityId": "AC-002",
          "name": "Leaning Activity 2",
          "location": "Camt_114",
          "date": "Wenesday",
          "time": 13.00,
          "teacher": "PP",
          "period_reg": "20 oct 2018",
          "description": "Lean every Wenesday"
        }
    ]
    
  
}
