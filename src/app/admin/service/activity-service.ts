import Activity from '../entity/activity';
import { Observable } from 'rxjs';

export abstract class ActivityService {
    abstract getActivitys(): Observable<Activity[]>;
    abstract getActivity(id: number): Observable<Activity>;
    abstract saveActivity(activity: Activity): Observable<Activity>;
}