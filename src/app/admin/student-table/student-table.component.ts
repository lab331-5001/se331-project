// import { Component, OnInit, ViewChild } from '@angular/core';
// import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
// import { Observable, of as observableOf, BehaviorSubject } from 'rxjs';
// import { StudentTableDataSource } from './student-table-datasource';

// @Component({
//   selector: 'app-Student-table',
//   templateUrl: './student-table.component.html',
//   styleUrls: ['./student-table.component.css']
// })
// export class StudentTableComponent implements OnInit {
//   @ViewChild(MatPaginator) paginator: MatPaginator;
//   @ViewChild(MatSort) sort: MatSort;
//   dataSource: StudentTableDataSource;
//   /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
//   displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'accept', 'reject'];
//   students: Student[];
//   filter: string;
//   filter$: BehaviorSubject<string>;
//   constructor(private studentService: StudentService) { }
//   ngOnInit() {
//     this.studentService.getStudents()
//       .subscribe(students => {
//         this.dataSource = new StudentTableDataSource(this.paginator, this.sort);
//         this.dataSource.data = students;
//         this.students = students;
//         this.filter$ = new BehaviorSubject<string>('');
//         this.dataSource.filter$ = this.filter$;
//       }
//       );
//   }
//   applyFilter(filterValue: string) {
//     this.filter$.next(filterValue.trim().toLowerCase());
//   }

//   accept(student: any) {
//       alert("You are accepted");
//       this.studentService.saveStudent(student);
//       this.studentService.deleteStudent(student);
//       this.router.navigate(['/hompage']);
//   }

//   reject(student: any){
//       alert("You are rejected");
//       this.studentService.deleteStudent(student);
//       this.router.navigate(['/homepage']);
//   }
