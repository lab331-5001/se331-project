import { Component, OnInit } from '@angular/core';
import Activity from '../../admin/entity/activity';
import { ActivityService } from '../../admin/service/activity-service';

@Component({
  selector: 'app-admin',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  activitys: Activity[];

  constructor(private activityService: ActivityService) { }
  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activitys => this.activitys = activitys);
  }

}
