import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivityTableDataSource } from './activity-table-datasource';
import { ActivityService } from '../../admin/service/activity-service';
import Activity from '../../admin/entity/activity';
import { Observable, of as observableOf, BehaviorSubject } from 'rxjs';
//import { FormBuilder }

@Component({
 selector: 'app-activity-table',
  templateUrl: './activity-table.component.html',
  styleUrls: ['./activity-table.component.css']
})
export class ActivityTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ActivityTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'activityId', 'name', 'location', 'date', 'time', 'teacher', 'period_reg', "description"];
  activitys: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService) { }
  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activitys => {
        this.dataSource = new ActivityTableDataSource(this.paginator, this.sort);
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  

}
