
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTableComponent } from './activity-table.component';

describe('ActivityableComponent', () => {
  let component: ActivityTableComponent;
  let fixture: ComponentFixture<ActivityTableComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActivityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
