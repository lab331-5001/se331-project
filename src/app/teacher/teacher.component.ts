import { Component, OnInit } from '@angular/core';
import Teacher from '../entity/teacher';
import { TeacherService } from '../service/teacher-service';
import { ActivatedRoute, RouteConfigLoadStart, Router } from '@angular/router';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
loginStudentCheck: boolean;
loginTeacherCheck: boolean;
modelTeacher: Teacher = new Teacher();
teachers: Teacher[] = [{
  'id': 1,
  'name': 'Ying',
  'surname': 'Somsri',
  'image': 'http://icons-for-free.com/free-icons/png/512/2754576.png',
  'username': 'kuu@cmu.ac.th',
  'password': 'kkkk'
}, {
  'id': 2,
  'name': 'Chai',
  'surname': 'Somchai',
  'image': 'http://icons-for-free.com/free-icons/png/512/2754582.png',
  'username': 'ajan@cmu.ac.th',
  'password': 'ajan'
}];
  constructor( private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  // onSubmitTeacher() {
  //   this.teacherService.getTeacherEmailAndPassword(this.modelTeacher.username, this.modelTeacher.password)
  //   .subscribe((teachers) => {
  //     if (teachers.id != null) {
  //       this.nav.show();
  //       this.nav.login();
  //       this.nav.setTeacher(teachers.id, teachers.name, teachers.surname, teachers.image);
  //       this.router.navigate(['/teacher']);
  //     } else {
  //       alert('Can not find your account');
  //     }
  //   }, (error) => {
  //     alert('Could not save value');
  //  });
loginStudent() {
  this.loginStudentCheck = true;
  this.loginTeacherCheck = false;
}
loginTeacher() {
  this.loginStudentCheck = false;
  this.loginTeacherCheck = true;
}
}
