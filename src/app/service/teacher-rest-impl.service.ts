import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TeacherService } from './teacher-service';
import { Observable, of } from 'rxjs';
import Teacher from '../entity/teacher';


@Injectable({
  providedIn: 'root'
})
export class TeacherRestImplService extends TeacherService {
  teacher: Teacher;
  constructor(private http: HttpClient) {
    super();
  }
  teachers: Teacher[] = [{
    'id': 1,
    'name': 'Ying',
    'surname': 'Somsri',
    'image': 'http://icons-for-free.com/free-icons/png/512/2754576.png',
    'username': 'kuu@cmu.ac.th',
    'password': 'kkkk'
  }, {
    'id': 2,
    'name': 'Chai',
    'surname': 'Somchai',
    'image': 'http://icons-for-free.com/free-icons/png/512/2754582.png',
    'username': 'ajan@cmu.ac.th',
    'password': 'ajan'
  }];

getTeacherEmailAndPassword(username: string, password: string): Observable<Teacher> {
const output: Teacher = this.teachers.find(teacher => teacher.username === username && teacher.password === password);
return of(this.teacher[output.id - 1]);
}
}
