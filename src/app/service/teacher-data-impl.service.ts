import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Teacher } from '../entity/teacher';
import { TeacherService } from './teacher-service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeacherDataImplService extends TeacherService {
  // constructor() {
  constructor(private http: HttpClient) {
    super();
   }

  teachers: Teacher[] = [
    {
      'id': 11,
      'teacherId': 'T-01',
      'name': 'Somjai',
      'surname': 'Medee',
      'subject': 'English',
      'image': 'assets/image/Teacher1.jpg'
    },
    {
      'id': 12,
      'teacherId': 'T-02',
      'name': 'Manee',
      'surname': 'Mana',
      'subject': 'Math',
      'image': 'assets/image/Teacher2.jpg'
    }
  ];
   getTeachers(): Observable<Teacher[]> {
    return of(this.teachers);
  }
  getTeacher(id: number): Observable<Teacher> {
    return of(this.teachers)
      .pipe(map(teachers => {
        const output: Teacher = (teachers as Teacher[]).find(teacher => teacher.id === +id);
        return output;
      }));
  }

  // getTeachers(): Observable<Teacher[]> {
  //   return this.http.get<Teacher[]>('');
  // }
  // saveTeacher(teacher: Teacher): Observable<Teacher> {
  //   return this.http.post<Teacher>('', this.teachers);
  // }
}
