import { TestBed } from '@angular/core/testing';

import { NavRestImplService } from './nav-rest-impl.service';

describe('NavRestImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavRestImplService = TestBed.get(NavRestImplService);
    expect(service).toBeTruthy();
  });
});
