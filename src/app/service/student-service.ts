import Student from '../entity/student';
import { Observable } from 'rxjs';

export abstract class StudentService {
  abstract getStudentEmailAndPassword(username: string, password: string): Observable<Student>;
}
