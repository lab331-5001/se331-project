import { Observable } from 'rxjs';
import Teacher from '../entity/teacher';

export abstract class TeacherService {
  abstract getTeacherEmailAndPassword(username: string, password: string): Observable<Teacher>;
  }


import { Observable } from 'rxjs';
import { Teacher } from '../entity/teacher';

export abstract class TeacherService {
  abstract getTeachers(): Observable<Teacher[]>;
  abstract getTeacher(id: number): Observable<Teacher>;
}
