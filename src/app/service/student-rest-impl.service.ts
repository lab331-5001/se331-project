import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import Student from '../entity/student';
import { HttpClient } from '@angular/common/http/http';


@Injectable({
  providedIn: 'root'
})
export class StudentRestImplService extends StudentService {
  students: Student[] = [{
    'id': 1,
    'studentId': 'Stu-001',
    'name': 'Ant',
    'surname': 'Mod',
    'image': 'https://imagessl5.casadellibro.com/m/ig/5/6033455.jpg',
    'birthday': '08/10/1997',
    'username': 'ant@hotmail.com',
    'password': '12345'
  }, {
    'id': 2,
    'studentId': 'Stu-002',
    'name': 'Bee',
    'surname': 'Pueng',
    'image': 'https://www.chortle.co.uk/images/photos/small/bee_movie.jpg',
    'birthday': '08/10/1997',
    'username': 'bee@hotmail.com',
    'password': '54321'
  }];
  constructor(private http: HttpClient) {
    super();
  }



  getStudentEmailAndPassword(username: string, password: string): Observable<Student> {
    const output: Student = this.students.find(student => student.username === username && student.password === password);
    return of(this.students[output.id - 1]);
  }
}
