import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatFormFieldModule,
MatTableModule, MatProgressSpinnerModule, MatInputModule,
 MatSortModule, MatPaginatorModule, MatGridListModule, MatMenuModule, MatCardModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { ActivityComponent } from './admin/list/activity.component';
import { ActivityTableComponent } from './admin/activity-table/activity-table.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ActivityRoutingModule } from './admin/activity-routing.module';
import { AdminRegisterActivityComponent } from './admin/registerActivity/regisActivity.component';
import { ActivityService } from './admin/service/activity-service';
import { ActivityFileImplService } from './admin/service/activity-file-impl.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { StudentTableComponent } from './admin/student-table/student-table.component';
import { SystemRoutingModule } from './system-routing.module';
import { TeacherComponent } from './teacher/teacher.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { StudentComponent } from './student/student.component';
import { AdminComponent } from './admin/admin.component';
import { TeacherService } from './service/teacher-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeacherDataImplService } from './service/teacher-data-impl.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatDatepickerModule
  , MatDatepickerToggle, MatFormFieldControl,
} from '@angular/material';
import { MatInputModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { MyNavComponent } from './my-nav/my-nav.component';
import { ActivityComponent } from './teachers/activity/activity.component';
import { ListComponent } from './teachers/list/list.component';
import { ViewComponent } from './teachers/view/view.component';
// import { TeachersComponent } from './teachers/Teachers.component';
import { TeacherRoutingModule } from './teachers/teacher-routing.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { TeacherTableComponent } from './teachers/teacher-table/teacher-table.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { AclistTableComponent } from './aclist-table/aclist-table.component';


@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    ActivityComponent,
    ListComponent,
    ViewComponent,
    TeacherTableComponent,
    // AclistTableComponent,
    // TeachersComponent
    AppComponent,
    MyNavComponent,
    ActivityComponent,
    ActivityTableComponent,
    AdminRegisterActivityComponent,
    // StudentTableComponent,
    TeacherComponent,
    LoginComponent,
    RegisterComponent,
    StudentComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSortModule,
    AppRoutingModule,
    TeacherRoutingModule,  // เพิ่มมา
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatPaginatorModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatDatepickerModule,
    // MatDatepickerToggle,
    MatTableModule,



    // ReactiveFormsModule
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatFormFieldModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatProgressSpinnerModule,
    ActivityRoutingModule,
    SystemRoutingModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {provide: TeacherService, useClass: TeacherDataImplService}
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  providers: [
    { provide: ActivityService, useClass: ActivityFileImplService }
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
