import { Component, OnInit } from '@angular/core';
import Student from '../entity/student';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

students: Student[] = [{
  'id': 1,
  'studentId': 'Stu-001',
  'name': 'Ant',
  'surname': 'Mod',
  'image': 'https://imagessl5.casadellibro.com/m/ig/5/6033455.jpg',
  'birthday': '08/10/1997',
  'username': 'ant@hotmail.com',
  'password': '12345'
}, {
  'id': 2,
  'studentId': 'Stu-002',
  'name': 'Bee',
  'surname': 'Pueng',
  'image': 'https://www.chortle.co.uk/images/photos/small/bee_movie.jpg',
  'birthday': '08/10/1997',
  'username': 'bee@hotmail.com',
  'password': '54321'
}];
  constructor() { }

  ngOnInit() {
  }

}
