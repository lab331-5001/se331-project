import { Component, OnInit } from '@angular/core';
import Student from '../entity/student';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
model: Student = new Student();
  constructor() { }

  ngOnInit() {
  }

}
