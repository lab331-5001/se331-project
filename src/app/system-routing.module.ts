import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ActivityTableComponent } from './admin/activity-table/activity-table.component';
import { TeacherComponent } from './teacher/teacher.component';


const systemRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'teacher', component: TeacherComponent},
  { path: 'list', component: ActivityTableComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(systemRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SystemRoutingModule {
}
